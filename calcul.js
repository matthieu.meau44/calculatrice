// Récupérer les éléments HTML nécessaires
const outputCurrent = document.querySelector(".currentCalcul");
const outputLast = document.querySelector(".lastCalcul");
const resetC = document.querySelector(".C");
const resetCE = document.querySelector(".CE");
const backspace = document.querySelector(".backspace");
const equals = document.querySelector(".equal");
const numbers = document.querySelectorAll(".calc");
const operands = document.querySelectorAll(".operande");

// Initialiser les variables
let current = "0";
let last = "";
let operand = "";

// Ajouter un gestionnaire d'événements pour chaque bouton de nombre
numbers.forEach((button) => {
  button.addEventListener("click", () => {
    // Si l'affichage courant est égal à 0, remplacer par le chiffre cliqué
    if (current === "0") {
      current = button.textContent;
    } else {
      current += button.textContent;
    }
    outputCurrent.textContent = current;
  });
});

// Ajouter un gestionnaire d'événements pour chaque bouton d'opérande
operands.forEach((button) => {
    button.addEventListener("click", () => {
    // Si une opérande existe déjà, effectuer le calcul
        if (operand) {
            calculate();
        }
    // Stocker l'opérande et le dernier affichage courant
        operand = button.textContent;
        last = current;
        current = "";
        outputLast.textContent = `${last} ${operand}`;
        outputCurrent.textContent = "0";
    });
});
// Ajouter un gestionnaire d'événement pour le bouton égal
equals.addEventListener("click", () => {
    calculate();
});
    
    // Ajouter un gestionnaire d'événement pour le bouton reset C
resetC.addEventListener("click", () => {
    current = "0";
    last = "";
    operand = "";
    outputLast.textContent = "";
    outputCurrent.textContent = current;
});
    
    // Ajouter un gestionnaire d'événement pour le bouton reset CE
resetCE.addEventListener("click", () => {
    current = "0";
    outputCurrent.textContent = current;
});

// Ajouter un gestionnaire d'événement pour le bouton backspace
backspace.addEventListener("click", () => {
    current = current.slice(0, -1);
    if (current === "") {
    current = "0";
    }
    outputCurrent.textContent = current;
    });
    
    // Définir la fonction de calcul
function calculate() {
    let result;
    switch (operand) {
    case "+":
        result = parseFloat(last) + parseFloat(current);
        break;
   
    case "-":
        result = parseFloat(last) - parseFloat(current);
        break;
    case "×":
        result = parseFloat(last) * parseFloat(current);
        break;
    case "÷":
        result = parseFloat(last) / parseFloat(current);
        break;
    default:
        return;
    }
        // Afficher le résultat
    current = result.toString();
    outputCurrent.textContent = current;
    outputLast.textContent = "";
    operand = "";
}
// Ajouter un gestionnaire d'événement pour les touches du clavier
document.addEventListener("keydown", (event) => {
    const key = event.key;
// Vérifier si la touche est un chiffre ou un point
    if (/^[0-9.]$/.test(key)) {
// Si l'affichage courant est égal à 0, remplacer par la touche pressée
        if (current === "0") {
            current = key;
        } else {
            current += key;
        }
        outputCurrent.textContent = current;
    }
    // Vérifier si la touche est une opérande
    else if (/^[+−×÷]$/.test(key)) {
    // Si une opérande existe déjà, effectuer le calcul
        if (operand) {
            calculate();
        }
        // Mettre à jour l'opérande et l'affichage du dernier calcul
        operand = key;
        last = current + " " + operand;
        outputLast.textContent = last;
        current = "0";
        outputCurrent.textContent = current;
    }
    // Vérifier si la touche est la touche "Entrée" pour effectuer le calcul
    else if (key === "Enter") {
        calculate();
    }
    // Vérifier si la touche est la touche "Suppr" ou "Backspace" pour effacer un chiffre
    else if (key === "Delete" || key === "Backspace") {
        if (current.length === 1) {
            current = "0";
        } else {
        current = current.slice(0, -1);
        }
        outputCurrent.textContent = current;
    }
});